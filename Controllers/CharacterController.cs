using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace TestASP.Controllers
{

    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class CharacterController : ControllerBase
    {
        private readonly ICharacterService _characterService;

        public CharacterController(ICharacterService characterService)
        {
            _characterService = characterService;
        }

        [HttpGet("getAll")]
        public async Task<ActionResult<ServicesResponse<GetCharacterDto>>> Get(){
            int userId = int.Parse(User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier)!.Value);
            return Ok(await _characterService.GetAllCharacters(userId));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ServicesResponse<GetCharacterDto>>> GetSingle(int id){
            return Ok(await _characterService.GetCharacterById(id));
        }


        
        [HttpPost]
        public async Task<ActionResult<ServicesResponse<List<GetCharacterDto>>>> AddCharacter(AddCharacterDto neweCharater)
        {
           
            return Ok(await _characterService.AddCharacter(neweCharater));
        }

        [HttpPut]
        public async Task<ActionResult<ServicesResponse<List<GetCharacterDto>>>> UpdateCharacter(UpdateCharacterDto updateCharater)
        {
           
            return Ok(await _characterService.UpdateCharacter(updateCharater));
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<ServicesResponse<GetCharacterDto>>> DeleteCharaceter(int id){
            var response = await _characterService.DeleteCharacter(id);
            if (response.Data is null){
                return NotFound(response);
            }
            return Ok(response);
        }


    }
}