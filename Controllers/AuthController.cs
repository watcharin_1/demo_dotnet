using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TestASP.Data;
using TestASP.Dtos.User;

namespace TestASP.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AuthController : ControllerBase
    {
        private readonly IAuthRepository _authRepository;
        public AuthController(IAuthRepository authRepository)
        {
            _authRepository = authRepository;
        }

        [HttpPost("register")]
        public async Task<ActionResult<ServicesResponse<int>>> Register(UserRegisterDto request){
            var response = await _authRepository.Register(
                new User { Username = request.Username }, request.Password
            );
            if(!response.Success){
                return BadRequest(response);
            }
            return Ok(response);
        }

            
        [HttpPost("login")]
        public async Task<ActionResult<ServicesResponse<int>>> Login(UserLoginDto request){
            var response = await _authRepository.Login(
                request.Username, request.Password
            );
            if(!response.Success){
                return BadRequest(response);
            }
            return Ok(response);
        }

    }
}