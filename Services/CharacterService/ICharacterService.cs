using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace TestASP.Services.CharacterService
{
    public interface ICharacterService
    {
        Task<ServicesResponse<List<GetCharacterDto>>> GetAllCharacters(int userId);
        Task<ServicesResponse<GetCharacterDto>> GetCharacterById(int id);
        Task<ServicesResponse<List<GetCharacterDto>>> AddCharacter(AddCharacterDto newCharacter);
        Task<ServicesResponse<GetCharacterDto>> UpdateCharacter(UpdateCharacterDto updatedCharacter);
        Task<ServicesResponse<List<GetCharacterDto>>> DeleteCharacter(int id);
        
    }
}