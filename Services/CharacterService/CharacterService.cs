
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestASP.Data;

namespace TestASP.Services.CharacterService
{
    public class CharacterService : ICharacterService
    {
        private static List<Character> characters = new List<Character>{
            new Character(),
            new Character{ Id = 1, Name = "Sam"}
        };
        private readonly IMapper _mapper;
        public readonly DataContext _context;

        public CharacterService(IMapper mapper, DataContext context)
        
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<ServicesResponse<List<GetCharacterDto>>> AddCharacter(AddCharacterDto newCharacter)
        {
            var servicesResponse = new ServicesResponse<List<GetCharacterDto>>();
            var character = _mapper.Map<Character>(newCharacter);
            _context.Characters.Add(character);

            characters.Add(character);
            await _context.SaveChangesAsync();

            servicesResponse.Data = await _context.Characters.Select(c => _mapper.Map<GetCharacterDto>(c)).ToListAsync();
            return servicesResponse;
        }

        public async Task<ServicesResponse<List<GetCharacterDto>>> DeleteCharacter(int id)
        {
            var servicesResponse = new ServicesResponse<List<GetCharacterDto>>();
            
            try{

                var character = await _context.Characters.FirstOrDefaultAsync(c => c.Id == id);
                if (character is null)
                    throw new Exception($"Character with id'{id}' not found");
                _context.Characters.Remove(character);
                await _context.SaveChangesAsync();
                characters.Remove(character);
                servicesResponse.Data = await _context.Characters.Select(c => _mapper.Map<GetCharacterDto>(c)).ToListAsync();
            }
            catch(Exception ex){
                servicesResponse.Success = false;
                servicesResponse.Message = ex.Message;        
            }
            
            return servicesResponse;
        }

        public async Task<ServicesResponse<List<GetCharacterDto>>> GetAllCharacters(int userId)
        {

            var ServicesResponse = new ServicesResponse<List<GetCharacterDto>>();
            var dbCharacters = await _context.Characters.Where(c => c.User!.Id == userId).ToListAsync();
            ServicesResponse.Data = dbCharacters.Select(c => _mapper.Map<GetCharacterDto>(c)).ToList();
            return ServicesResponse;
        }

        public async Task<ServicesResponse<GetCharacterDto>> GetCharacterById(int id)
        {
            var ServicesResponse = new ServicesResponse<GetCharacterDto>();
            var dbCharacters = await _context.Characters.FirstOrDefaultAsync(c => c.Id == id);
            var character = characters.FirstOrDefault(c => c.Id == id);
            ServicesResponse.Data = _mapper.Map<GetCharacterDto>(character);

            return ServicesResponse;
        
        }

        public async Task<ServicesResponse<GetCharacterDto>> UpdateCharacter(UpdateCharacterDto updatedCharacter)
        {
            var servicesResponse = new ServicesResponse<GetCharacterDto>();

            try{

                var character = await _context.Characters.FirstOrDefaultAsync(c => c.Id == updatedCharacter.Id);
                if (character is null)
                    throw new Exception($"Character with id'{updatedCharacter.Id}' not found");
                    
                character.Name = updatedCharacter.Name;
                character.HitPoints = updatedCharacter.HitPoints;
                character.Defense = updatedCharacter.Defense;
                character.Intelligence = updatedCharacter.Intelligence;
                character.Strength = updatedCharacter.Strength;
                character.Class = updatedCharacter.Class;
                
                await _context.SaveChangesAsync();
                servicesResponse.Data = _mapper.Map<GetCharacterDto>(character);

            }
            catch(Exception ex){
                servicesResponse.Success = false;
                servicesResponse.Message = ex.Message;        
            }
            
            return servicesResponse;
        }


    }
}