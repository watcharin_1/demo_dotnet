using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestASP.Data
{
    public interface IAuthRepository
    {
        Task<ServicesResponse<int>> Register(User user, string password);
        Task<ServicesResponse<string>> Login( string username, string password);
        Task<bool> UserExists(string username);
    }
}